Set file = CreateObject("Scripting.FileSystemObject")
Set term = CreateObject("WScript.Shell")

Const TemporaryFolder = 2
xfile = WScript.ScriptFullName
lfile = file.BuildPath(file.GetSpecialFolder(TemporaryFolder),"MSYS2.lnk")
mingw = file.GetParentFolderName(xfile)
title = "MinGW64 Shell (MSYS2) Setup"
label = "MY1MSYS2"
l4b3l = "MinGW64 Shell"

x_key = "HKCU\Software\Classes\Directory\background\shell\"
y_key = "HKCU\Software\Classes\Directory\shell\"
m_key = x_key+label+"\"
c_key = y_key+label+"\"
d_bug = 0

If WScript.Arguments.Length > 0 Then
	Set link = term.CreateShortcut(lfile)
	link.TargetPath = file.BuildPath(mingw,"usr\bin\mintty.exe")
	link.IconLocation = file.BuildPath(mingw,"msys2.ico")
	If d_bug Then term.Popup "Arg: "+WScript.Arguments(0),,title,64+4096
	If file.FolderExists(WScript.Arguments(0)) Then
		link.WorkingDirectory = WScript.Arguments(0)
	Else
		link.WorkingDirectory = term.CurrentDirectory
	End If
	link.Arguments = "/bin/sh -lc 'cd $(cygpath """+link.WorkingDirectory+""");exec bash'"
	If d_bug Then term.Popup "WD: "+link.WorkingDirectory,,title,64+4096
	link.Save
	Call RunIt
Else
	Call Setup
End If

Call Cleanup

Sub Setup
	Dim errnum
	errnum = 0
	On Error Resume Next
	check = term.RegRead(m_key+"command\")
	If Err.Number = 0 Then
		check = Mid(check,9,Len(check)-11)
	End If
	On Error GoTo 0
	If d_bug Then
		term.Popup "Check: "+check+" ("+m_key+"command\)",,title,64+4096
	End If
	If Not file.FileExists(check) Then
		If term.Popup("Install Folder context menu?",,title,4+32+4096) <> 6 Then
			Call Cleanup
		End If
		On Error Resume Next
		term.RegWrite m_key,l4b3l
		errnum = errnum + Err.Number
		term.RegWrite m_key+"command\","WScript "+xfile+" 01"
		errnum = errnum + Err.Number
		term.RegWrite c_key,l4b3l
		errnum = errnum + Err.Number
		term.RegWrite c_key+"command\","WScript "+xfile+" %1"
		errnum = errnum + Err.Number
		' need the following to maintain default
		term.RegWrite y_key,"Open"
		errnum = errnum + Err.Number
		If errnum <> 0 Then
			term.Popup "Failed to install folder context menu",,title,64+4096
		Else
			term.Popup "Folder context menu installed",,title,64+4096
		End If
		On Error GoTo 0
	Else
		If term.Popup("Remove Folder context menu?",,title,4+32+4096) = 6 Then
			On Error Resume Next
			term.RegDelete m_key+"command\"
			errnum = errnum + Err.Number
			term.RegDelete m_key
			errnum = errnum + Err.Number
			term.RegDelete c_key+"command\"
			errnum = errnum + Err.Number
			term.RegDelete c_key
			errnum = errnum + Err.Number
			If errnum <> 0 Then
				term.Popup "Failed to remove folder context menu",,title,64+4096
			Else
				term.Popup "Folder context menu removed",,title,64+4096
			End If
			On Error GoTo 0
		End If
	End If
End Sub

Sub RunIt
	Set apps = CreateObject("Shell.Application")
	apps.ShellExecute lfile
End Sub

Sub Cleanup
	Set term = Nothing
	Set file = Nothing
	WScript.Quit
End Sub
