mingw4user
==========

This is a collection of Windows batch files (*.bat) to prepare MinGW-based Windows development environment (console-based, NO IDE!) for standard users using standard account - it does not need any admin account.

Simply copy these files to the path where MinGW packages are extracted (can be manually done). Double-click on mingw-setup.bat to setup an environment. Tools folder provide mingw-desk.bat to create destop shortcut, and mingw-term.bat for the user to copy to a project folder (and later simply click to get a development environment in that folder). Double-click on mingw-clean.bat to 'clean up' (i.e. remove) all the created settings file(s).

A new VB script, msys.vbs, has been added to create/delete a context menu item that allows users to start an MSYS shell at any location selected from the Windows explorer (need to comment out any cd command at the end of etc/profile). This script writes to the registry but still does not need an admin account. It writes to the HKEY_CURRENT_USER root.

I will provide a better description some time in the future.... as usual, depending on my laziness factor :p
