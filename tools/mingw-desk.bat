@echo off
REM script to create desktop shortcut in WinXP
REM will be calling another vbscript that actually do the hard work

REM -----------------------------
REM CHECK FOR MINGW INSTALLATION!
REM -----------------------------

call mingw-check.bat
if exist "%mingw%" goto MINGW_OK
echo Cannot find MinGW folder!
pause
goto END

REM ------------------------
REM CREATE DESKTOP SHORTCUT!
REM ------------------------

:MINGW_OK
set thisdir=%~dp0
set thisdir=%thisdir:~0,-1%

cd /d %thisdir%
cscript wsh-script.txt //e:vbscript
