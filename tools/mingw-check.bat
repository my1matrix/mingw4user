@echo off
REM the main script to 'setup' and 'cleanup' path information
REM to be called by other batch file(s)

REM -------------------------
REM MINGW INSTALL PARAMETERS!
REM -------------------------

set MINGW_STARTENV=setup-mingw.bat
set MINGW_APPSNAME=MinGW
set MINGW_USERPATH=%APPDATA%\%MINGW_APPSNAME%
set MINGW_PATHFILE=%MINGW_USERPATH%\mingw.path

REM -----------------------
REM CHECK CALLING FUNCTION!
REM -----------------------

if defined MINGW_SETUP goto SET_REC
if defined MINGW_CLRUP goto CLR_REC

REM --------------------------
REM BY DEFAULT, SETUP %MINGW%!
REM --------------------------

if not exist "%MINGW_PATHFILE%" goto END
set /p mingw=<"%MINGW_PATHFILE%"
REM remove the newline char!
set mingw=%mingw:~0,-1%
if exist "%mingw%" goto END
set mingw=
goto END

REM -----------------
REM MINGW PATH SETUP!
REM -----------------

:SET_REC
if defined mingw goto WRITE_PATH
echo Setup error: MinGW path not defined!
echo Run '%MINGW_STARTENV%' file from installation folder!
pause
goto END
:WRITE_PATH
if not exist "%MINGW_USERPATH%" goto MAKE_PATHREC
if not exist "%MINGW_PATHFILE%" goto MAKE_FILEREC
echo %MINGW_APPSNAME% info/record has been written!
goto END
:MAKE_PATHREC
mkdir "%MINGW_USERPATH%"
echo Directory '%MINGW_USERPATH%' created!
if not exist "%MINGW_PATHFILE%" goto MAKE_FILEREC
pause
goto END
:MAKE_FILEREC
echo %mingw% > "%MINGW_PATHFILE%"
echo File '%MINGW_PATHFILE%' created!
echo System ready for %MINGW_APPSNAME%!
pause
goto END

REM -------------------
REM MINGW INFO CLEANUP!
REM -------------------

:CLR_REC
if exist "%MINGW_PATHFILE%" goto ERASE_FILEREC
if exist "%MINGW_USERPATH%" goto ERASE_PATHREC
echo %MINGW_APPSNAME% info/record are not found!
pause
goto END
:ERASE_FILEREC
del "%MINGW_PATHFILE%"
echo File '%MINGW_PATHFILE%' removed!
if exist "%MINGW_USERPATH%" goto ERASE_PATHREC
pause
goto END
:ERASE_PATHREC
rmdir "%MINGW_USERPATH%"
echo Directory '%MINGW_USERPATH%' removed!
echo System cleaned from %MINGW_APPSNAME%!
pause

:END

REM ----------------------
REM CLEANUP ENV VARIABLES!
REM ----------------------

set MINGW_STARTENV=
set MINGW_APPSNAME=
set MINGW_USERPATH=
set MINGW_PATHFILE=
