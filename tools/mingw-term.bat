@echo off
REM standard mingw development environment!
REM can be copied to any directory to start a console

REM -----------------------------
REM CHECK FOR MINGW INSTALLATION!
REM -----------------------------

set MINGW_APPSNAME=MinGW
set MINGW_USERPATH=%APPDATA%\%MINGW_APPSNAME%
set MINGW_PATHFILE=%MINGW_USERPATH%\mingw.path

if exist "%MINGW_PATHFILE%" goto FIND_PATH
echo Missing %MINGW_PATHFILE%!
echo Aborting!
pause
goto END

:FIND_PATH
set /p mingw=<"%MINGW_PATHFILE%"
set mingw=%mingw:~0,-1%
if exist "%mingw%" goto MINGW_OK
echo Cannot find MinGW folder!
pause
goto END

REM ----------------------
REM SET MINGW ENVIRONMENT!
REM ----------------------

:MINGW_OK
set mingw_path=%mingw%\bin;%mingw%\mingw32\bin;%mingw%\tools
set path=%mingw_path%;%path%

REM ---------------------
REM CHECK GETPATH OPTION!
REM ---------------------

if defined MINGWTERM_GETPATH goto END

REM --------------------
REM START MINGW CONSOLE!
REM --------------------

:START_CONSOLE
start "MinGW Console" /max /normal cmd

:END

REM ----------------------
REM CLEANUP ENV VARIABLES!
REM ----------------------

set MINGW_APPSNAME=
set MINGW_USERPATH=
set MINGW_PATHFILE=
